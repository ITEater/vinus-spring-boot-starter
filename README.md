# 针对阿里云组件封装的一个神奇starter： vinus-spring-boot-starter

## TODO 新增了日志处理相关的，最近忙没时间写文档……后面更新



#### 2019-12-09更新

1. 添加人机验证模块（captcha），主要用于阿里云数据风控（业务安全）模块的后台pc端配置，详情请看：[阿里云人机验证说明文档](https://help.aliyun.com/product/28308.html)
2. 版本号升级为0.4.1

#### 介绍

针对阿里云的产品进行的spring boot starter的封装，目前只封装了afs（人机验证，captcha）、sms（短信验证）、oss（对象存储）、sts（访问控制）四个模块，后续有需求我会进一步的添加，对于我来说sms与oss两个模块目前用的最多。所以这块也做了一些封装


#### 软件架构

###### 版本需求

当前工程版本：0.4.1

![jdk版本](https://img.shields.io/badge/java-1.8%2B-red.svg?style=for-the-badge&logo=appveyor)
![maven版本](https://img.shields.io/badge/maven-3.2.5%2B-red.svg?style=for-the-badge&logo=appveyor)
![spring boot](https://img.shields.io/badge/spring%20boot-2.0.0.RELEASE%2B-red.svg?style=for-the-badge&logo=appveyor)

![aliyun-core](https://img.shields.io/badge/aliyun%20core-4.2.5-green.svg?style=for-the-badge&logo=appveyor)
![aliyun-oss](https://img.shields.io/badge/aliyun%20oss-3.5.0-green.svg?style=for-the-badge&logo=appveyor)
![aliyun-sts](https://img.shields.io/badge/aliyun%20sms-1.1.0-green.svg?style=for-the-badge&logo=appveyor)

###### 系统配置

* 系统配置是按照“你需要什么就配置什么”的原则来进行相关配置，所以对于不同的模块就需要不同的配置：所以对于配置，目前分为四大块：captcha配置，oss配置，sms配置，sts配置
* 所有配置基本上都会包含三个数据：1、accessKeyId 2、accessKey Secret 3、regionId（选填），以上的三个数据是每个可编程访问模块所必须的数据，这三项数据由阿里云的访问控制中创建子账号进行提供的，对于访问控制请看：[访问控制](https://help.aliyun.com/document_detail/28627.html?spm=a2c8b.12215325.0.0.211e62539G4AH2)
* captchaafs配置主要包括：appKey应用秘钥、scene验证类型
* oss配置主要包括：默认的bucket、endpoint、oss代理过期时间、bucket与域名绑定的映射4项
* sms配置主要包括：endpoint、sms的产品类型（目前定死为Dysmsapi）、短信发送的域名（目前定死dysmsapi.aliyuncs.com）
* sts配置主要包括：阿里云资源与角色对照信息、角色会话名称、临时授权的过期时间

#### 安装教程

1. 下载此工程，通过``mvn intall``后，引入其依赖：

```

		<dependency>
			<groupId>com.kuding</groupId>
			<artifactId>vinus-spring-boot-starter</artifactId>
			<version>0.4.1</version>
		</dependency>
		
```

2. 在``appplication.properties``或``application.yml``文件中进行相关配置


3. 加入captcha人机验证模块，则需要做如下配置：

```

vinus.enable-aliyun-captcha=true
aliyun.captcha.access-key-id=你自己的accessId
aliyun.captcha.secret=你自己的access-secret
aliyun.captcha.region-id=cn-hangzhou
aliyun.captcha.app-key=你自己的appKey
aliyun.captcha.sene=登录验证方式方式


```

4. 加入需要引入短信模块，则需要做如下配置：

```

vinus.enable-aliyun-sms=true
aliyun.sms.access-key-id=你自己的accessId
aliyun.sms.secret=你自己的access-secret
aliyun.sms.region-id=cn-hangzhou

```

5.  若需要引入oss模块，则需要引入如下配置：

```
vinus.enable-aliyun-oss=true
aliyun.oss.access-key-id=你自己的accessId
aliyun.oss.secret=你自己的access-secret
aliyun.oss.default-bucket=my-bucket
aliyun.oss.policy-timeout=30m
aliyun.oss.end-point=oss-cn-xxx.aliyuncs.com
```

6. sts模块暂时先不讲了，有机会再说



#### 使用说明





对于应用也非常的简单，核心的类为XxxComonent。阿里云的afs（captcha）的核心类为AliCaptchaComponent、阿里云sms的核心类为SmsComponent、阿里云oss的核心类为OssComponent、阿里云Sts核心类为StsComponent;

* 对于captcha（afs）组件，核心方法为``public boolean check(HttpServletRequest httpServletRequest, CaptchaContent capcha)``方法，通过httpservletrequest来进行处理请求的相关信息，CaptchaContent是用于验证的一些关键参数，目前只支持pcweb端的验证。对于人机验证的相关的文档请参看：[https://help.aliyun.com/product/28308.html](https://help.aliyun.com/product/28308.html)


* 对于sms组件，核心的方法为``public void doSendSms(String phone, String smsSignature, String templateCode, Map<String, String> map)``方法，参数依次为：手机号、短信签名、短信模板code、和参数（map结构）；对于短信签名一般都是固定的，对于templateCode的管理建议可以通过enum的方式进行管理

```
public enum SmsTemplate {

	MODIFY_PWD("SMS_120270002"), CREATE_USER("SMS_172223677"), RESET_PWD("SMS_162524221"), INIT_SAAS("SMS_162546827");;

	private final String template;

	public String getTemplate() {
		return template;
	}

	private SmsTemplate(String template) {
		this.template = template;
	}

}
```

*  ossComponent对于我而言是最常用的组件，所以对于oss的封装也是最全面的。对于大部分情况而言，oss上传需要web端进行直传，所以需要服务端进行获取一个临时授权策略给web端，web端可以通过授权策略记性web端直传，具体流程可以查看阿里云官方文档：[https://help.aliyun.com/document_detail/112718.html](https://help.aliyun.com/document_detail/112718.html?spm=a2c4g.11186623.6.1381.3de849e8wssfWB)
临时授权策略需要的信息已封装到类中：``OssPolicy``，用法也很简单：

```
	@PostMapping("/createExplainPicPolicy")
	@ApiOperation("创建一个说明图片上传的临时策略")
	public ResultModel<OssPolicy> createExplainPicPolicy() {
		OssPolicy ossPolicy = ossComponent.createOssPolicy("文件目录", "my-bucket");
		return ResponseStatusEnum.SUCCESS.createResultModel(ossPolicy);
	}

```
第一个参数表示是哪个文件目录，第二个参数表示是哪个bucket


* 当然对于oss，有一些情况是需要服务端进行上传信息的，这一部分非常的灵活，oss不仅可以上传文件，还可以上传任何形式的文本（json、xml、html等）假如你上传的是json，通过oss获取也是标准的restful风格的接口的话，支付要在Oss配置需要的header（例如content-type）即可，如:

```
	/**
	 * 将一个字符串上传到oss
	 * 
	 * @param object
	 * @param contentType
	 * @param path
	 * @return
	 */
	public String putStringToOss(String object, ContentType contentType, String path) 

```




#### 说在后面

* 这个框架也比较简单，思想上主要是把阿里云的组件进行模块化开发，这样的好处就是方便与其他的框架（spring cloud）整合并方便配合阿里云控制台进行管理。我在这也算是抛砖引玉，对于有阿里云其他模块开发的需求，可以留言给我，我看看如何进行添加。
* 最近比较忙，此框架的文档我也会不定期的进行完善。


[![Fork me on Gitee](https://gitee.com/ITEater/vinus-spring-boot-starter/widgets/widget_2.svg)](https://gitee.com/ITEater/vinus-spring-boot-starter)
