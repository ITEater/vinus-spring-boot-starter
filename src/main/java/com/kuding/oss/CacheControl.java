package com.kuding.oss;

public enum CacheControl {

	NO_CACHE("no-cache"), NO_STORE("no-store"), PUBLIC("public"), PRIVITE("private"), NO_TRANSFORM("no-transform");

	private final String value;

	private CacheControl(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
