package com.kuding.models;

import java.time.Duration;

public class OssPolicy {

	private String accessId;

	private String policy;

	private String signature;

	private String dir;

	private String host;

	private String maintainHost;

	private Long expire;

	private String bucket;

	public OssPolicy(String bucket, String accessId, String policy, String signature, String dir, String host,
			String maintainHost, Duration expire) {
		this.bucket = bucket;
		this.accessId = accessId;
		this.policy = policy;
		this.signature = signature;
		this.dir = dir;
		this.host = host;
		this.maintainHost = maintainHost;
		this.expire = System.currentTimeMillis() + expire.toMillis();
	}

	public String getAccessId() {
		return accessId;
	}

	public void setAccessId(String accessId) {
		this.accessId = accessId;
	}

	public String getPolicy() {
		return policy;
	}

	public void setPolicy(String policy) {
		this.policy = policy;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Long getExpire() {
		return expire;
	}

	public void setExpire(Long expire) {
		this.expire = expire;
	}

	public String getBucket() {
		return bucket;
	}

	public void setBucket(String bucket) {
		this.bucket = bucket;
	}

	public String getMaintainHost() {
		return maintainHost;
	}

	public void setMaintainHost(String maintainHost) {
		this.maintainHost = maintainHost;
	}

}
