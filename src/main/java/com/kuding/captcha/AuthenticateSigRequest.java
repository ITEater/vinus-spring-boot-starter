package com.kuding.captcha;

import com.aliyuncs.RpcAcsRequest;

/**
 * @author auto create
 * @version
 */
public class AuthenticateSigRequest extends RpcAcsRequest<AuthenticateSigResponse> {

	public AuthenticateSigRequest() {
		super("afs", "2018-01-12", "AuthenticateSig");
	}

	private String sig;

	private Long resourceOwnerId;

	private String remoteIp;

	private String sourceIp;

	private String appKey;

	private String sessionId;

	private String token;

	private String scene;

	public String getSig() {
		return this.sig;
	}

	public void setSig(String sig) {
		this.sig = sig;
		if (sig != null) {
			putQueryParameter("Sig", sig);
		}
	}

	public Long getResourceOwnerId() {
		return this.resourceOwnerId;
	}

	public void setResourceOwnerId(Long resourceOwnerId) {
		this.resourceOwnerId = resourceOwnerId;
		if (resourceOwnerId != null) {
			putQueryParameter("ResourceOwnerId", resourceOwnerId.toString());
		}
	}

	public String getRemoteIp() {
		return this.remoteIp;
	}

	public void setRemoteIp(String remoteIp) {
		this.remoteIp = remoteIp;
		if (remoteIp != null) {
			putQueryParameter("RemoteIp", remoteIp);
		}
	}

	public String getSourceIp() {
		return this.sourceIp;
	}

	public void setSourceIp(String sourceIp) {
		this.sourceIp = sourceIp;
		if (sourceIp != null) {
			putQueryParameter("SourceIp", sourceIp);
		}
	}

	public String getAppKey() {
		return this.appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
		if (appKey != null) {
			putQueryParameter("AppKey", appKey);
		}
	}

	public String getSessionId() {
		return this.sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
		if (sessionId != null) {
			putQueryParameter("SessionId", sessionId);
		}
	}

	public String getToken() {
		return this.token;
	}

	public void setToken(String token) {
		this.token = token;
		if (token != null) {
			putQueryParameter("Token", token);
		}
	}

	public String getScene() {
		return this.scene;
	}

	public void setScene(String scene) {
		this.scene = scene;
		if (scene != null) {
			putQueryParameter("Scene", scene);
		}
	}

	@Override
	public Class<AuthenticateSigResponse> getResponseClass() {
		return AuthenticateSigResponse.class;
	}

}
