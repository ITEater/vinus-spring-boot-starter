package com.kuding.captcha;

import com.aliyuncs.transform.UnmarshallerContext;

public class AuthenticateSigResponseUnmarshaller {

	public static AuthenticateSigResponse unmarshall(AuthenticateSigResponse authenticateSigResponse,
			UnmarshallerContext context) {

		authenticateSigResponse.setRequestId(context.stringValue("AuthenticateSigResponse.RequestId"));
		authenticateSigResponse.setCode(context.integerValue("AuthenticateSigResponse.Code"));
		authenticateSigResponse.setMsg(context.stringValue("AuthenticateSigResponse.Msg"));
		authenticateSigResponse.setRiskLevel(context.stringValue("AuthenticateSigResponse.RiskLevel"));
		authenticateSigResponse.setDetail(context.stringValue("AuthenticateSigResponse.Detail"));

		return authenticateSigResponse;
	}
}