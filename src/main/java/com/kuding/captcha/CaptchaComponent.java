package com.kuding.captcha;

import javax.servlet.http.HttpServletRequest;

@FunctionalInterface
public interface CaptchaComponent<T> {

	public boolean check(HttpServletRequest httpServletRequest, T capcha);
}
