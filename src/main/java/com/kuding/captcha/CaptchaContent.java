package com.kuding.captcha;

public class CaptchaContent {

	private String sign;

	private String scene;

	private String sessionId;

	private String token;

	public CaptchaContent(String sign, String scene, String sessionId, String token) {
		this.sign = sign;
		this.scene = scene;
		this.sessionId = sessionId;
		this.token = token;
	}

	public CaptchaContent() {
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getScene() {
		return scene;
	}

	public void setScene(String scene) {
		this.scene = scene;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		return "CapchaContent [sign=" + sign + ", scene=" + scene + ", sessionId=" + sessionId + ", token=" + token
				+ "]";
	}

}
