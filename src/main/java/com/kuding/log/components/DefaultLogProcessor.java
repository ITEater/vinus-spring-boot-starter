package com.kuding.log.components;

import java.util.Map;

import com.kuding.log.interfaces.VinusLogProcessAdpter;

public class DefaultLogProcessor implements VinusLogProcessAdpter {

	private static final String DEFAULT_GROUP = "default";

	@Override
	public String getGroup() {
		return DEFAULT_GROUP;
	}

	@Override
	public void process(Map<String, String> content) {
		content.forEach((x, y) -> System.out.println(x.concat("\t:\t").concat(y)));
	}

}
