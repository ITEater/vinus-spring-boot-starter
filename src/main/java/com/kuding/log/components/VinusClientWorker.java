package com.kuding.log.components;

import java.util.concurrent.ExecutorService;

import com.aliyun.openservices.loghub.client.ClientWorker;
import com.aliyun.openservices.loghub.client.config.LogHubConfig;
import com.aliyun.openservices.loghub.client.exceptions.LogHubClientWorkerException;
import com.aliyun.openservices.loghub.client.interfaces.ILogHubProcessorFactory;
import com.kuding.log.interfaces.ConsumerGroup;
import com.kuding.log.utils.CurrentGroupUtils;

public class VinusClientWorker extends ClientWorker implements ConsumerGroup {

	private final String consumerGroup;

	public VinusClientWorker(ILogHubProcessorFactory factory, LogHubConfig config, ExecutorService service,
			String consumerGroup) throws LogHubClientWorkerException {
		super(factory, config, service);
		this.consumerGroup = consumerGroup;

	}

	@Override
	public void run() {
		CurrentGroupUtils.add(consumerGroup);
		super.run();
	}

	@Override
	public String getGroup() {
		return consumerGroup;
	}

}
