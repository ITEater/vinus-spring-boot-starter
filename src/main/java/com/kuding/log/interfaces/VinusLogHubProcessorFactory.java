package com.kuding.log.interfaces;

import java.util.List;
import java.util.Map;

import com.aliyun.openservices.loghub.client.interfaces.ILogHubProcessor;
import com.aliyun.openservices.loghub.client.interfaces.ILogHubProcessorFactory;
import com.kuding.log.components.DefaultLogProcessor;
import com.kuding.log.components.DefaultVinusLogHubProcessor;
import com.kuding.log.utils.CurrentGroupUtils;

public class VinusLogHubProcessorFactory implements ILogHubProcessorFactory {

	private final Map<String, List<VinusLogProcessAdpter>> processAdapterMap;

	private final List<VinusLogProcessAdpter> defaultProcessor = List.of(new DefaultLogProcessor());

	public VinusLogHubProcessorFactory(Map<String, List<VinusLogProcessAdpter>> processAdapterMap) {
		this.processAdapterMap = processAdapterMap;
	}

	@Override
	public ILogHubProcessor generatorProcessor() {
		String groupName = CurrentGroupUtils.get();
		VinusLogHubProcessor vinusLogHubProcessor = new DefaultVinusLogHubProcessor(
				processAdapterMap.getOrDefault(groupName, defaultProcessor), groupName);
		return vinusLogHubProcessor;
	}
}
