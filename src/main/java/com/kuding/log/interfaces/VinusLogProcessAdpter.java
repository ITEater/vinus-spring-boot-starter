package com.kuding.log.interfaces;

import java.util.Map;

public interface VinusLogProcessAdpter extends ConsumerGroup {

	public void process(Map<String, String> content);

}
