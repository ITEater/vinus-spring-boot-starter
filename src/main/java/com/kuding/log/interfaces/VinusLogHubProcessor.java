package com.kuding.log.interfaces;

import java.util.List;

import com.aliyun.openservices.loghub.client.interfaces.ILogHubProcessor;

public interface VinusLogHubProcessor extends ILogHubProcessor, ConsumerGroup {

	List<VinusLogProcessAdpter> getProcessors();
}
