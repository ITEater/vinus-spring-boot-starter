package com.kuding.log.utils;

public final class CurrentGroupUtils {

	private static final ThreadLocal<String> currentGroup = ThreadLocal.withInitial(() -> "default");

	public static void add(String groupName) {
		currentGroup.set(groupName);
	}

	public static String get() {
		return currentGroup.get();
	}

	public static void clear() {
		currentGroup.remove();
	}
}
