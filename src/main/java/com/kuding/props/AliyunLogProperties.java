package com.kuding.props;

import java.util.Collections;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "vinus.aliyun.log")
public class AliyunLogProperties extends ALiYunCoreProperty {

	/**
	 * 日志服务访问节点
	 */
	private String endpoint;

	/**
	 * 日志的logStore
	 */
	private Map<String, AliyunLogStoreProperties> logStores = Collections.emptyMap();

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public Map<String, AliyunLogStoreProperties> getLogStores() {
		return logStores;
	}

	public void setLogStores(Map<String, AliyunLogStoreProperties> logStores) {
		this.logStores = logStores;
	}

}
