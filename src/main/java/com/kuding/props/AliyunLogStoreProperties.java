package com.kuding.props;

import java.time.Duration;

import com.aliyun.openservices.loghub.client.config.LogHubConfig;
import com.aliyun.openservices.loghub.client.config.LogHubConfig.ConsumePosition;

public class AliyunLogStoreProperties {

	/**
	 * 消费者名称，同组名的条件下，不同工程下，名称不同
	 */
	private String consumer = "consumer-0";

	/**
	 * 日志的工程名
	 */
	private String logProject;

	/**
	 * 日志的存储store
	 */
	private String logStore;

	/**
	 * 每次发送日志的最大数量
	 */
	private Integer logMaxNumPerFetch = 100;

	/**
	 * 发送日志的间隔时间
	 */
	private Duration logFetchDataInterval = Duration.ofSeconds(2);

	/**
	 * 每次调用listShards的间隔时间
	 */
	private Duration logShardsDiscoveryInterval = Duration.ofSeconds(30);

	/**
	 * 心跳检查间隔
	 */
	private Duration heartbeatInterval = Duration.ofSeconds(5);

	/**
	 * 是否按顺序消费
	 */
	private boolean consumeInOrder = false;

	/**
	 * 日志消费的起始位置
	 */
	private ConsumePosition logConsumerBeginPosition = ConsumePosition.END_CURSOR;

	/**
	 * 处理的数据时数据容量最大值,0为无限制
	 * 
	 */
	private int maxInProgressingDataSizeInMB = 0;

	/**
	 * 连接过期时间
	 */
	private Duration connectionTimeout = Duration.ofMinutes(1);

	/**
	 * 是否自动提交检查点
	 */
	private boolean autoCommitEnbaled = true;

	/**
	 * 自动提交检查点的时间间隔
	 */
	private Duration autoCommitInterval = Duration.ofMinutes(1);

	/**
	 * 上传检查点以后，把consumer清空
	 * 
	 */
	private boolean unloadAfterCommitEnabled = false;

	/**
	 * 用户标识，可以不管
	 */
	private String userAgent;

	/**
	 * 用于起始时间的定义，若设置为大于0，则会重载logConsumerBeginPosition的设置
	 */
	private int startTimestamp = 0;

	public LogHubConfig generateLogHubConfig(AliyunLogProperties aliyunLogProperties, String consumerGroup) {
		LogHubConfig logHubConfig = null;
		if (startTimestamp == 0)
			logHubConfig = new LogHubConfig(consumerGroup, consumer, aliyunLogProperties.getEndpoint(), logProject,
					logStore, aliyunLogProperties.getAccessKeyId(), aliyunLogProperties.getSecret(),
					logConsumerBeginPosition, logMaxNumPerFetch);
		else
			logHubConfig = new LogHubConfig(consumerGroup, consumer, aliyunLogProperties.getEndpoint(), logProject,
					logStore, aliyunLogProperties.getAccessKeyId(), aliyunLogProperties.getSecret(), startTimestamp,
					logMaxNumPerFetch);
		logHubConfig.setAutoCommitEnabled(autoCommitEnbaled);
		logHubConfig.setConsumeInOrder(consumeInOrder);
		logHubConfig.setAutoCommitIntervalMs(autoCommitInterval.toMillis());
		logHubConfig.setFetchIntervalMillis(logFetchDataInterval.toMillis());
		logHubConfig.setHeartBeatIntervalMillis(heartbeatInterval.toMillis());
		logHubConfig.setMaxInProgressingDataSizeInMB(maxInProgressingDataSizeInMB);
		logHubConfig.setTimeoutInSeconds(Long.valueOf(connectionTimeout.toSeconds()).intValue());
		return logHubConfig;
	}

	public String getConsumer() {
		return consumer;
	}

	public void setConsumer(String consumer) {
		this.consumer = consumer;
	}

	public String getLogProject() {
		return logProject;
	}

	public void setLogProject(String logProject) {
		this.logProject = logProject;
	}

	public String getLogStore() {
		return logStore;
	}

	public void setLogStore(String logStore) {
		this.logStore = logStore;
	}

	public Integer getLogMaxNumPerFetch() {
		return logMaxNumPerFetch;
	}

	public void setLogMaxNumPerFetch(Integer logMaxNumPerFetch) {
		this.logMaxNumPerFetch = logMaxNumPerFetch;
	}

	public Duration getLogFetchDataInterval() {
		return logFetchDataInterval;
	}

	public void setLogFetchDataInterval(Duration logFetchDataInterval) {
		this.logFetchDataInterval = logFetchDataInterval;
	}

	public Duration getLogShardsDiscoveryInterval() {
		return logShardsDiscoveryInterval;
	}

	public void setLogShardsDiscoveryInterval(Duration logShardsDiscoveryInterval) {
		this.logShardsDiscoveryInterval = logShardsDiscoveryInterval;
	}

	public Duration getHeartbeatInterval() {
		return heartbeatInterval;
	}

	public void setHeartbeatInterval(Duration heartbeatInterval) {
		this.heartbeatInterval = heartbeatInterval;
	}

	public boolean isConsumeInOrder() {
		return consumeInOrder;
	}

	public void setConsumeInOrder(boolean consumeInOrder) {
		this.consumeInOrder = consumeInOrder;
	}

	public ConsumePosition getLogConsumerBeginPosition() {
		return logConsumerBeginPosition;
	}

	public void setLogConsumerBeginPosition(ConsumePosition logConsumerBeginPosition) {
		this.logConsumerBeginPosition = logConsumerBeginPosition;
	}

	public int getMaxInProgressingDataSizeInMB() {
		return maxInProgressingDataSizeInMB;
	}

	public void setMaxInProgressingDataSizeInMB(int maxInProgressingDataSizeInMB) {
		this.maxInProgressingDataSizeInMB = maxInProgressingDataSizeInMB;
	}

	public Duration getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(Duration connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public boolean isAutoCommitEnbaled() {
		return autoCommitEnbaled;
	}

	public void setAutoCommitEnbaled(boolean autoCommitEnbaled) {
		this.autoCommitEnbaled = autoCommitEnbaled;
	}

	public Duration getAutoCommitInterval() {
		return autoCommitInterval;
	}

	public void setAutoCommitInterval(Duration autoCommitInterval) {
		this.autoCommitInterval = autoCommitInterval;
	}

	public boolean isUnloadAfterCommitEnabled() {
		return unloadAfterCommitEnabled;
	}

	public void setUnloadAfterCommitEnabled(boolean unloadAfterCommitEnabled) {
		this.unloadAfterCommitEnabled = unloadAfterCommitEnabled;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public int getStartTimestamp() {
		return startTimestamp;
	}

	public void setStartTimestamp(int startTimestamp) {
		this.startTimestamp = startTimestamp;
	}

}
