package com.kuding.props;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "vinus.aliyun.captcha")
public class AliyunCaptchaProperties extends ALiYunCoreProperty {

	/**
	 * appId
	 */
	private String appKey;

	/**
	 * 类型
	 */
	private String sene;

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getSene() {
		return sene;
	}

	public void setSene(String sene) {
		this.sene = sene;
	}

	@Override
	public String toString() {
		return "AliyunCapchaProperties [appKey=" + appKey + ", sene=" + sene + "]";
	}

}
