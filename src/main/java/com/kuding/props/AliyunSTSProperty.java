package com.kuding.props;

import java.time.Duration;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "vinus.aliyun.sts")
public class AliyunSTSProperty extends ALiYunCoreProperty {

	/**
	 * 节点信息
	 */
	private String endPoint = "sts.aliyuncs.com";

	/**
	 * 阿里云的角色资源名称
	 */
	private Map<RamRoleFunction, String> roleArns;

	/**
	 * 你的角色会话名称
	 */
	private String RoleSessionName = "session-name";

	/**
	 * 阿里云临时授权的过期时间
	 */
	private Duration expireTime = Duration.ofMinutes(5);

	/**
	 * @return the endPoint
	 */
	public String getEndPoint() {
		return endPoint;
	}

	/**
	 * @param endPoint the endPoint to set
	 */
	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}

	/**
	 * @return the roleArns
	 */
	public Map<RamRoleFunction, String> getRoleArns() {
		return roleArns;
	}

	/**
	 * @param roleArns the roleArns to set
	 */
	public void setRoleArns(Map<RamRoleFunction, String> roleArns) {
		this.roleArns = roleArns;
	}

	/**
	 * @return the expireTime
	 */
	public Duration getExpireTime() {
		return expireTime;
	}

	/**
	 * @param expireTime the expireTime to set
	 */
	public void setExpireTime(Duration expireTime) {
		this.expireTime = expireTime;
	}

	public String getRoleSessionName() {
		return RoleSessionName;
	}

	public void setRoleSessionName(String roleSessionName) {
		RoleSessionName = roleSessionName;
	}

}
