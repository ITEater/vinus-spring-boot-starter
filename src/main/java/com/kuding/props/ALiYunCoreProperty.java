package com.kuding.props;

public class ALiYunCoreProperty {

	protected String regionId;
	protected String accessKeyId;
	protected String secret;
	protected boolean enabled = false;

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	public String getRegionId() {
		return regionId;
	}

	public String getAccessKeyId() {
		return accessKeyId;
	}

	public String getSecret() {
		return secret;
	}

	public void setAccessKeyId(String accessKeyId) {
		this.accessKeyId = accessKeyId;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return "ALiYunCoreProperty [regionId=" + regionId + ", accessKeyId=" + accessKeyId + ", secret=" + secret
				+ ", enabled=" + enabled + "]";
	}

}