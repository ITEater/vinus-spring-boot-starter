package com.kuding.props;

import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("vinus.aliyun.oss")
public class AliYunOssProperty extends ALiYunCoreProperty {

	/**
	 * 默认的bucket
	 */
	private String defaultBucket;

	/**
	 * 节点地址
	 */
	private String endPoint;

	/**
	 * 代理过期时间
	 */
	private Duration policyTimeout;

	/**
	 * 绑定域名，主要替换bucket与endpoint结合形成的域名
	 */
	private Map<String, String> domainNames = new LinkedHashMap<>();

	public String getDefaultBucket() {
		return defaultBucket;
	}

	public void setDefaultBucket(String defaultBucket) {
		this.defaultBucket = defaultBucket;
	}

	public String getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}

	/**
	 * @return the policyTimeout
	 */
	public Duration getPolicyTimeout() {
		return policyTimeout;
	}

	/**
	 * @param policyTimeout the policyTimeout to set
	 */
	public void setPolicyTimeout(Duration policyTimeout) {
		this.policyTimeout = policyTimeout;
	}

	/**
	 * @return the domainNames
	 */
	public Map<String, String> getDomainNames() {
		return domainNames;
	}

	/**
	 * @param domainNames the domainNames to set
	 */
	public void setDomainNames(Map<String, String> domainNames) {
		this.domainNames = domainNames;
	}

	public String getMainUrl() {
		return getMainUrl(defaultBucket);
	}

	public String getMainUrl(String bucket) {
		String endPointSplit = endPoint.replaceFirst("https?://", "");
		String url = String.format("https://%s.%s", bucket, endPointSplit);
		return url;
	}

	@Override
	public String toString() {
		return "AliYunOssProperty [defaultBucket=" + defaultBucket + ", endPoint=" + endPoint + ", policyTimeout="
				+ policyTimeout + ", domainNames=" + domainNames + ", regionId=" + regionId + ", accessKeyId="
				+ accessKeyId + ", secret=" + secret + "]";
	}

}
