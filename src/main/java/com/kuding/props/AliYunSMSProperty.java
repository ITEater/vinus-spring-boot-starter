package com.kuding.props;

import java.time.Duration;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("vinus.aliyun.sms")
public class AliYunSMSProperty extends ALiYunCoreProperty {

	/**
	 * 节点信息
	 */
	private String endPoint = "dysmsapi.aliyuncs.com";

	/**
	 * 连接过期时间
	 */
	private Duration connectionTimeout = Duration.ofSeconds(10);

	/**
	 * 读取过期时间
	 */
	private Duration readTimeout = Duration.ofSeconds(10);

	public String getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}

	public Duration getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(Duration connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public Duration getReadTimeout() {
		return readTimeout;
	}

	public void setReadTimeout(Duration readTimeout) {
		this.readTimeout = readTimeout;
	}

	@Override
	public String toString() {
		return "AliYunSMSProperty [endPoint=" + endPoint + ", connectionTimeout=" + connectionTimeout + ", readTimeout="
				+ readTimeout + ", regionId=" + regionId + ", accessKeyId=" + accessKeyId + ", secret=" + secret
				+ ", enabled=" + enabled + "]";
	}
	

	
}
