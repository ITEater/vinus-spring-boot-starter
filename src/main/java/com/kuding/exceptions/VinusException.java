package com.kuding.exceptions;

public class VinusException extends RuntimeException {

	private static final long serialVersionUID = -6428127707945874530L;

	public VinusException() {
		super();
	}

	public VinusException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public VinusException(String message, Throwable cause) {
		super(message, cause);
	}

	public VinusException(String message) {
		super(message);
	}

	public VinusException(Throwable cause) {
		super(cause);
	}
}
