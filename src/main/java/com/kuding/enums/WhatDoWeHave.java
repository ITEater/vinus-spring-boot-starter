package com.kuding.enums;

public enum WhatDoWeHave {

	CAPTCHA("captcha"), ECS("ecs"), LOG("log"), OSS("oss"), SMS("sms"), STS("sts");

	private final String value;

	private WhatDoWeHave(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
