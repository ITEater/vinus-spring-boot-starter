package com.kuding.enums;

public enum AliyunSmsApiErrorCode {

	OK("OK", "调用成功"), EXTEND_CODE_ERROR("isv.EXTEND_CODE_ERROR", "扩展码使用错误，相同的扩展码不可用于多个签名"),
	DOMESTIC_NUMBER_NOT_SUPPORTED("isv.DOMESTIC_NUMBER_NOT_SUPPORTED", "国际/港澳台消息模板不支持发送境内号码"),
	DAY_LIMIT_CONTROL("isv.DAY_LIMIT_CONTROL", "日发送已达上限"),
	SMS_CONTENT_ILLEGAL("isv.SMS_CONTENT_ILLEGAL", "短信内容包含禁止发送内容"), SMS_SIGN_ILLEGAL("isv.SMS_SIGN_ILLEGAL", "签名禁止使用"),
	RAM_PERMISSION_DENY("isp.RAM_PERMISSION_DENY", "权限错误"), OUT_OF_SERVICE("isv.OUT_OF_SERVICE", "业务停机"),
	PRODUCT_UN_SUBSCRIPT("isv.PRODUCT_UN_SUBSCRIPT", "产品未开通"), ACCOUNT_NOT_EXISTS("isv.ACCOUNT_NOT_EXISTS", "账户不存在"),
	ACCOUNT_ABNORMAL("isv.ACCOUNT_ABNORMAL", "账户异常"), SMS_TEMPLATE_ILLEGAL("isv.SMS_TEMPLATE_ILLEGAL", "短信模板不合法"),
	SMS_SIGNATURE_ILLEGAL("isv.SMS_SIGNATURE_ILLEGAL", "短信签名不合法"), INVALID_PARAMETERS("isv.INVALID_PARAMETERS", "参数异常"),
	SYSTEM_ERROR("isp.SYSTEM_ERROR", "短信系统错误"), MOBILE_NUMBER_ILLEGAL("isv.MOBILE_NUMBER_ILLEGAL", "非法手机号"),
	MOBILE_COUNT_OVER_LIMIT("isv.MOBILE_COUNT_OVER_LIMIT", "手机号数量超限"),
	TEMPLATE_MISSING_PARAMETERS("isv.TEMPLATE_MISSING_PARAMETERS", "缺少模板参数"),
	BUSINESS_LIMIT_CONTROL("isv.BUSINESS_LIMIT_CONTROL", "业务限流"),
	INVALID_JSON_PARAM("isv.INVALID_JSON_PARAM", "json参数不合法"),
	BLACK_KEY_CONTROL_LIMIT("isv.BLACK_KEY_CONTROL_LIMIT", "黑名单限制"),
	PARAM_LENGTH_LIMIT("isv.PARAM_LENGTH_LIMIT", "参数超过长度限制"),
	PARAM_NOT_SUPPORT_URL("isv.PARAM_NOT_SUPPORT_URL", "不支持的url"), AMOUNT_NOT_ENOUGH("isv.AMOUNT_NOT_ENOUGH", "余额不足"),
	TEMPLATE_PARAMS_ILLEGAL("isv.TEMPLATE_PARAMS_ILLEGAL", "模版变量里包含非法关键字");

	private final String code;

	private final String message;

	private AliyunSmsApiErrorCode(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public static String getMessage(String code) {
		if (code != null) {
			for (AliyunSmsApiErrorCode aliyunSmsApiErrorCode : AliyunSmsApiErrorCode.values()) {
				if (aliyunSmsApiErrorCode.code.equals(code)) {
					return aliyunSmsApiErrorCode.message;
				}
			}
		}
		return null;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
