package com.kuding.sts.policy;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Policy {

	@SerializedName("Version")
	private String version = "1";

	@SerializedName("Statement")
	private List<Statement> statement;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public List<Statement> getStatement() {
		return statement;
	}

	public void setStatement(List<Statement> statement) {
		this.statement = statement;
	}

}
