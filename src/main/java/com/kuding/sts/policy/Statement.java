package com.kuding.sts.policy;

import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class Statement {

	@SerializedName("Effect")
	private String effect;

	@SerializedName("Action")
	private String[] action;

	@SerializedName("Resource")
	private String[] resource;

	@SerializedName("Condition")
	private Map<String, String[]> condition;

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public String[] getAction() {
		return action;
	}

	public void setAction(String[] action) {
		this.action = action;
	}

	public String[] getResource() {
		return resource;
	}

	public void setResource(String[] resource) {
		this.resource = resource;
	}

	public Map<String, String[]> getCondition() {
		return condition;
	}

	public void setCondition(Map<String, String[]> condition) {
		this.condition = condition;
	}

}
