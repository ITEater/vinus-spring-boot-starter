package com.kuding.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.kuding.config.conditional.ConditionalOnEnableAliyun;
import com.kuding.enums.WhatDoWeHave;
import com.kuding.props.AliyunSTSProperty;
import com.kuding.sts.StsComponent;

@Configuration
@ConditionalOnEnableAliyun(WhatDoWeHave.STS)
@EnableConfigurationProperties({ AliyunSTSProperty.class })
public class AliyunStsComponentConfig {

	@Autowired
	private AliyunSTSProperty aliyunSTSProperty;

	@Bean
	public StsComponent stsComponent() {
		StsComponent stsComponent = new StsComponent(aliyunSTSProperty);
		return stsComponent;
	}
}
