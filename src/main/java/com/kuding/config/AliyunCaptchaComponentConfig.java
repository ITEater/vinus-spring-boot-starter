package com.kuding.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.kuding.captcha.AliCaptchaComponent;
import com.kuding.config.conditional.ConditionalOnEnableAliyun;
import com.kuding.enums.WhatDoWeHave;
import com.kuding.props.AliyunCaptchaProperties;

@Configuration
@EnableConfigurationProperties({ AliyunCaptchaProperties.class })
@ConditionalOnWebApplication
@ConditionalOnEnableAliyun(WhatDoWeHave.CAPTCHA)
public class AliyunCaptchaComponentConfig {

	@Autowired
	private AliyunCaptchaProperties aliyunCaptchaProperties;

	@Bean
	@ConditionalOnMissingBean
	public AliCaptchaComponent aliCapchaComponent() {
		AliCaptchaComponent aliCapchaComponent = new AliCaptchaComponent(aliyunCaptchaProperties);
		return aliCapchaComponent;
	}
}
