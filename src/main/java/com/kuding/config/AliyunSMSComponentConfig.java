package com.kuding.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.aliyun.dysmsapi20170525.Client;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuding.config.conditional.ConditionalOnEnableAliyun;
import com.kuding.enums.WhatDoWeHave;
import com.kuding.props.AliYunSMSProperty;
import com.kuding.sms.SmsComponent;

@Configuration
@ConditionalOnEnableAliyun(WhatDoWeHave.SMS)
@ConditionalOnClass({ Client.class })
@EnableConfigurationProperties({ AliYunSMSProperty.class })
public class AliyunSMSComponentConfig {

	@Bean
	public SmsComponent aliyunSmsComponent(AliYunSMSProperty aliYunSMSProperty, ObjectMapper objectMapper) {
		SmsComponent aliyunSmsComponent = new SmsComponent(objectMapper, aliYunSMSProperty);
		return aliyunSmsComponent;
	}
}
