package com.kuding.config.conditional;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Conditional;

import com.kuding.enums.WhatDoWeHave;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(ComponentEnabledConditional.class)
public @interface ConditionalOnEnableAliyun {

	WhatDoWeHave value();

}
