package com.kuding.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.aliyuncs.ecs.model.v20140526.DescribeInstancesRequest;
import com.kuding.config.conditional.ConditionalOnEnableAliyun;
import com.kuding.ecs.components.EcsComponent;
import com.kuding.enums.WhatDoWeHave;
import com.kuding.props.AliyunEcsProperties;

@Configuration
@ConditionalOnEnableAliyun(WhatDoWeHave.ECS)
@ConditionalOnClass({ DescribeInstancesRequest.class })
@EnableConfigurationProperties({ AliyunEcsProperties.class })
public class AliyunEcsComponentConfig {

	@Bean
	@ConditionalOnMissingBean
	public EcsComponent ecsComponent(AliyunEcsProperties aliyunEcsProperties) {
		EcsComponent ecsComponent = new EcsComponent(aliyunEcsProperties);
		return ecsComponent;
	}
}
