package com.kuding.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.kuding.config.conditional.ConditionalOnEnableAliyun;
import com.kuding.enums.WhatDoWeHave;
import com.kuding.oss.OssComponent;
import com.kuding.props.AliYunOssProperty;

@Configuration
@ConditionalOnEnableAliyun(WhatDoWeHave.OSS)
@EnableConfigurationProperties({ AliYunOssProperty.class })
public class AliyunOSSComponentConfig {

	@Autowired
	private AliYunOssProperty aliYunOssProperty;

	@Bean
	public OssComponent ossComponent() {
		OssComponent ossComponent = new OssComponent(aliYunOssProperty);
		return ossComponent;
	}
}
